const ambiente = process.env.NODE_ENV || 'development';

const configuracionBase = {
  jwt: {},
  puerto: process.env.PORT,
  suprimirLogs: false,
  pathEnBucket: 'imagenes'
};

let configuracionDeAmbiente = {};

switch (ambiente) {
  case 'desarrollo':
  case 'dev':
  case 'development':
    configuracionDeAmbiente = require('./dev');
    break;
  case 'producción':
  case 'production':
  case 'prod':
    configuracionDeAmbiente = require('./prod');
    break;
  default:
    configuracionDeAmbiente = require('./dev');
}

module.exports = {
  ...configuracionBase,
  ...configuracionDeAmbiente
};
