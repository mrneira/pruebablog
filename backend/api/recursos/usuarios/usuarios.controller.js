const Usuario = require('./usuarios.model');

function obtenerUsuarios() {
  return Usuario.find({});
}

function crearUsuario(usuario, hashedPassword) {
  return new Usuario({
    ...usuario,
    password: hashedPassword
  }).save();
}

function usuarioExiste(username, email) {
  return new Promise((resolve, reject) => {
    Usuario.find()
      .or([{ username: username }, { email: email }])
      .then(usuarios => {
        resolve(usuarios.length > 0);
      })
      .catch(err => {
        reject(err);
      });
  });
}

function obtenerUsuario(
  { username: username, id: id, email: email },
  usuarioLoggeadoId
) {
  if (username) {
    return obtenerUsuarioConQuery({ username: username }, usuarioLoggeadoId);
  } else if (id) {
    return obtenerUsuarioConQuery({ _id: id });
  } else if (email) {
    return obtenerUsuarioConQuery({ email: email });
  } else {
    throw new Error(
      'Función obtener usuario del controller fue llamada sin especificar username, email o id.'
    );
  }
}

async function obtenerUsuarioConQuery(query) {
  const usuario = await Usuario.findOne(query);
  return usuario;
}


module.exports = {
  obtenerUsuarios,
  crearUsuario,
  usuarioExiste,
  obtenerUsuario
};
