const mongoose = require('mongoose');
const ObjectId = mongoose.Schema.Types.ObjectId;

const postSchema = new mongoose.Schema(
  {
    url: {
      type: String,
      required: [
        true,
        'Post debe tener URL de la imagen donde puede ser descargada'
      ]
    },
    usuario: {
      type: ObjectId,
      required: [true, 'Post debe estar asociada a un usuario'],
      ref: 'usuario',
      index: true
    },
    caption: {
      type: String,
      maxlength: 180
    }
  },
  {
    timestamps: { createdAt: 'fecha_creado', updatedAt: 'fecha_actualizado' }
  }
);

const Post = mongoose.model('post', postSchema);

module.exports = {
  Post
};
