const AWS = require('aws-sdk');
const fs = require('fs').promises
const { Post } = require('./posts.model');
const config = require('../../../config');



function obtenerPosts() {
  return Post.find({})
    .populate('usuario', '_id username')
    .sort({ fecha_creado: -1 });
}

function obtenerFeed(buscarAntesDeFecha) {
  const posts = Post.find({ fecha_creado: { $lt: buscarAntesDeFecha } })
    .sort({ fecha_creado: -1 })
    .limit(3)
    .populate('usuario', '_id username imagen');
  return posts;
}

function obtenerPostsParaUsuario(usuarioId) {
  return Post.find({ usuario: usuarioId });
}

function obtenerPost(id) {
  return Post.findById(id)
    .populate('usuario', '_id username imagen')
    .then(post => {
      if (!post) {
        let err = new Error(`Post con id [${id}] no existe.`);
        err.status = 404;
        throw err;
      }
      return post;
    });
}

function crearPost(post, usuarioId) {
  return new Post({
    ...post,
    usuario: usuarioId
  }).save();
}

async function guardarImagen(imageData, nombreDelArchivo) {
  // Guardar imagenes en la carpeta /public/imagenes
  await fs.writeFile(`${__dirname}/../../../public/imagenes/${nombreDelArchivo}`, imageData)
  return `/imagenes/${nombreDelArchivo}`
}

module.exports = {
  obtenerPosts,
  obtenerPost,
  crearPost,
  guardarImagen,
  obtenerPostsParaUsuario,
  obtenerFeed
};
