require('dotenv').config();
const express = require('express');
const bodyParser = require('body-parser');
const morgan = require('morgan');
const mongoose = require('mongoose');
const postsRouter = require('./api/recursos/posts/posts.routes');
const usuariosRouter = require('./api/recursos/usuarios/usuarios.routes');
const logger = require('./helpers/logger');
const authJWT = require('./api/helpers/auth');
const config = require('./config');
const errorHandler = require('./api/helpers/errorHandler');

const passport = require('passport');
passport.use(authJWT);

mongoose.connect(process.env.MONGODB_URI, {
  useNewUrlParser: true,
  useFindAndModify: false,
  useCreateIndex: true,
  useUnifiedTopology: true
});
mongoose.connection.on('error', () => {
  logger.error('Falló la conexión a mongodb');
  process.exit(1);
});

const app = express();
app.use(bodyParser.json());
app.use(bodyParser.raw({ type: 'image/*', limit: '8mb' }));
app.use(
  morgan('short', {
    stream: {
      write: message => logger.info(message.trim())
    }
  })
);

// Servir archivos estáticos que están en la carpeta public/
app.use(express.static('public'));



app.use(passport.initialize());

app.use('/api/usuarios', usuariosRouter);
app.use('/api/posts', [postsRouter]);

app.use(errorHandler.procesarErroresDeDB);
app.use(errorHandler.procesarErroresDeTamanioDeBody);
if (config.ambiente === 'prod') {
  app.use(errorHandler.erroresEnProduccion);
} else {
  app.use(errorHandler.erroresEnDesarrollo);
}

let server;

if (process.env.NODE_ENV === 'development') {
  server = app.listen(config.puerto, () => {
    logger.info(`Escuchando en el puerto ${config.puerto}.`);
  });
} else {
  app.listen();
}

module.exports = {
  app,
  server
};
