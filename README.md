# Instruciones de ejecución
1. Tener instalado reactJS en su ordenador.
2. Dirigirse por consola al directorio /frontend/
3. Ejecutar el comando npm i
4. Ejecutar el comando npm start 

# NOTA
El backend se encuentra subido en un server con ip publica; Por tal razón, solo se debe ejecutar las instrucciones que se describen previamente.
