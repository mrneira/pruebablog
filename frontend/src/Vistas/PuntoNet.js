import React from 'react';
import Main from '../Componentes/Main';
import { Link } from 'react-router-dom';

export default function Remote({ mostrarError, usuario }) {
    return (
        <Main center={true}>
            <div className="NoSiguesANadie">
                <p className="NoSiguesANadie__mensaje">Actualmente no existe ningún Post que se utilice tecnología .NET. De último momento me toco viajar fuera de la ciudad.</p>
                <div className="text-center">
                    <Link to="/" className="NoSiguesANadie__boton">Post locales</Link>
                </div>
            </div>
        </Main>
    );
}